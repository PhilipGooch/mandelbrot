#pragma once
#include <SFML/Graphics.hpp>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <amp.h>
#include <time.h>
#include <string>
#include <array>
#include <amp_math.h>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <complex>
#include "input.h"

const int WIDTH = 1920;
const int HEIGHT = 1080;
const int MAX_ITERATIONS = 500;
const int TS_X = 128;
const int TS_Y = 8;
const int ACCELERATOR_ID = 0;

#define MULTI_GPU 0

struct Complex1 
{
	double x;
	double y;
};

class Mandelbrot
{
public:
	Mandelbrot(sf::RenderWindow* window, Input* input);
	~Mandelbrot();

	sf::RenderWindow* window_;
	Input* input_;

	bool running_ = true;


	const long double left_ = -1.5;
	const long double right_ = 1.5;
	const long double top_ = 1.125;
	const long double bottom_ = -1.125;

	int delta_mouse_x_ = 0;
	int delta_mouse_y_ = 0;

	long double x_ = -1.2417935910153268;
	long double y_ = 0.32340979014235266;

	long double min_zoom_ = 0;
	long double max_zoom_ = -0.99999999999999623;

	long double zoom_ = min_zoom_;

	long double speed_;


	sf::Image* image_;
	sf::Texture* texture_;
	sf::Sprite* sprite_;

	void HandleInput();

	void Execute();

#if MULTI_GPU
	void Compute(double x, double y, double zoom);
#else
	void Compute(double left, double right, double top, double bottom);
#endif

	void Convert();

	void Render();

	double Map(long double value, long double min1, long double max1, long double min2, long double max2);

	void report_accelerator(const concurrency::accelerator a);

	void list_accelerators();

	void query_AMP_support();

};

