#include "mandelbrot.h"

uint32_t image_data_2D[HEIGHT][WIDTH];

std::vector<sf::Uint8>* image_data_1D;

Complex1 c_add(Complex1 c1, Complex1 c2) restrict(cpu, amp) // restrict keyword - able to execute this function on the GPU and CPU
{
	Complex1 tmp;
	double a = c1.x;
	double b = c1.y;
	double c = c2.x;
	double d = c2.y;
	tmp.x = a + c;
	tmp.y = b + d;
	return tmp;
} // c_add

Complex1 c_mul(Complex1 c1, Complex1 c2) restrict(cpu, amp)
{
	Complex1 tmp;
	double a = c1.x;
	double b = c1.y;
	double c = c2.x;
	double d = c2.y;
	tmp.x = a * c - b * d;
	tmp.y = b * c + a * d;
	return tmp;
} // c_mul

#if 0
uint32_t makeABGR(float r, float g, float b) restrict(amp)
{
	return (int)(r * 255) << 16 | (int)(g * 255) << 8 | (int)(b * 255);
}


uint32_t colourHSVtoABGR(float h, float s, float v) restrict(amp)
{
	float i;
	while (h >= 360.0f)
		h -= 360.0f;
	h /= 60.f;
	//i = (float)floor(h);
	int h_int = (int)h;
	i = (float)h_int;
	float f = h - i;
	float p = v * (1 - s);
	float q = v * (1 - (s * f));
	float t = v * (1 - (s * (1 - f)));
	switch ((int)i)
	{
	case 0: return makeABGR(v, t, p);
	case 1: return makeABGR(q, v, p);
	case 2: return makeABGR(p, v, t);
	case 3: return makeABGR(p, q, v);
	case 4: return makeABGR(t, p, v);
	case 5: return makeABGR(v, p, q);
	}
	return 0;
}
#endif

Mandelbrot::Mandelbrot(sf::RenderWindow* window, Input* input) :
	window_(window),
	input_(input)
{
	image_data_1D = new std::vector<sf::Uint8>();
	image_data_1D->assign(HEIGHT * WIDTH * 4, 0);
	image_ = new sf::Image();
	texture_ = new sf::Texture();
	sprite_ = new sf::Sprite();

	query_AMP_support();

#if MULTI_GPU
	Compute(x_, y_, zoom_);
#else
	Compute(left_, right_, top_, bottom_);
#endif
	image_->create(WIDTH, HEIGHT, image_data_1D->data());
	texture_->loadFromImage(*image_);
	sprite_->setTexture(*texture_);

	speed_ = Map(1 + zoom_, 1, 0, 0.1, 0);

	Execute();
}

Mandelbrot::~Mandelbrot()
{
}

void Mandelbrot::HandleInput()
{
	if (input_->isKeyDown(sf::Keyboard::Escape))
	{
		running_ = false;
	}

#if 1
	delta_mouse_x_ = (input_->getMouseX()) - 960;
	delta_mouse_y_ = (input_->getMouseY()) - 540;
	//delta_mouse_x_ = (input_->getMouseX()) - 320;
	//delta_mouse_y_ = (input_->getMouseY()) - 240;
#else
	delta_mouse_x_ = (input_->getMouseX()) - 314;
	delta_mouse_y_ = (input_->getMouseY()) - 225;
#endif

	//if (!(delta_mouse_x_ == 0 && delta_mouse_y_ == 0))
	{
		x_ += delta_mouse_x_ * speed_ *0.01;
		y_ -= delta_mouse_y_ * speed_ *0.01;

		if (input_->isMouseLeftDown())
		{
			if (zoom_ > max_zoom_)
			{
				speed_ = Map(1 + zoom_, 1, 0, 0.1, 0);
				zoom_ -= speed_;
			}
		}
		if (input_->isMouseRightDown())
		{
			if (zoom_ < 0)
			{
				speed_ = Map(1 + zoom_, 1, 0, 0.1, 0);
				zoom_ += speed_;
			}
		}

		Execute();
	}
}

void Mandelbrot::Execute()
{
	//std::cout << zoom_  << "\n"; 
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
#if MULTI_GPU
	Compute(x_, y_, zoom_);
#else
	Compute(x_ + left_ + left_ * zoom_, x_ + right_ + right_ * zoom_, y_ + top_ + top_ * zoom_, y_ + bottom_ + bottom_ * zoom_);
#endif
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	//std::cout << "compute:\t" << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << std::endl;
	start = std::chrono::steady_clock::now();
	Convert();
	end = std::chrono::steady_clock::now();
	//std::cout << "convert:\t" << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << std::endl;
	start = std::chrono::steady_clock::now();
	image_->create(WIDTH, HEIGHT, image_data_1D->data());
	texture_->loadFromImage(*image_);
	sprite_->setTexture(*texture_);
	Render();
	end = std::chrono::steady_clock::now();
	//std::cout << "render:\t\t" << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << std::endl << std::endl;
}

#if MULTI_GPU
void Mandelbrot::Compute(double x, double y, double zoom)
{
	concurrency::accelerator acc_0(concurrency::accelerator::default_accelerator);
	concurrency::accelerator_view view_0 = acc_0.default_view;

	concurrency::extent<2> ext(int(HEIGHT / accelerators.size()), WIDTH);

	concurrency::array<int, 2> top_half(ext, image_data_2D[0], view_0);

#if 1
	double top_half_left = x + -1.5 + -1.5 * zoom;
	double top_half_right = x + 1.5 + 1.5 * zoom;
	double top_half_top = y + 1.125 + 1.125 * zoom;
	double top_half_bottom = y + -1.125 + -1.125 * zoom;

	concurrency::parallel_for_each(top_half.extent, [=, &top_half](concurrency::index<2> t_idx) restrict(amp)
		{
			int x = t_idx[1];
			int y = t_idx[0];

			// Work out the point in the complex plane that
			// corresponds to this pixel in the output image.
			Complex1 c;
			c.x = top_half_left + (x * (top_half_right - top_half_left) / WIDTH);
			c.y = top_half_top + (y * (top_half_bottom - top_half_top) / HEIGHT);

			// Start off z at (0, 0).
			Complex1 z;
			z.x = 0;
			z.y = 0;

			// Iterate z = z^2 + c until z moves more than 2 units
			// away from (0, 0), or we've iterated too many times.
			int iterations = 0;

			while ((z.x * z.x) + (z.y * z.y) < 2.0 * 2.0 && iterations < MAX_ITERATIONS)
			{
				z = c_add(c_mul(z, z), c);

				++iterations;
			}
			if (iterations == MAX_ITERATIONS)
			{

				// z didn't escape from the circle.
				// This point is in the Mandelbrot set.
				top_half(y, x) = 0xFFFF0000;
			}
			else
			{
				// z escaped within less than MAX_ITERATIONS
				// iterations. This point isn't in the set.
				//av_image(y, x) = ;
				top_half(y, x) = 0xFF000000 + (iterations << 16);
				//av_image(y, x) = colourHSVtoABGR(iterations, 0.8, 0.8);
			}
		});
	concurrency::copy(top_half, image_data_2D[0]);


	double bottom_half_left = x + -1.5 + -1.5 * zoom;
	double bottom_half_right = x + 1.5 + 1.5 * zoom;
	double bottom_half_top = y - 1.125 * (1 + zoom) + 1.125 + 1.125 * zoom;
	double bottom_half_bottom = y  - 1.125 * (1 + zoom) + -1.125 + -1.125 * zoom;

	concurrency::accelerator acc_1(concurrency::accelerator::default_accelerator);
	concurrency::accelerator_view view_1 = acc_1.default_view;

	concurrency::array<int, 2> bottom_half(ext, image_data_2D[int(HEIGHT / accelerators.size())], view_1);

	concurrency::parallel_for_each(bottom_half.extent, [=, &bottom_half](concurrency::index<2> t_idx) restrict(amp)
	{
		int x = t_idx[1];
		int y = t_idx[0];

		// Work out the point in the complex plane that
		// corresponds to this pixel in the output image.
		Complex1 c;
		c.x = bottom_half_left + (x * (bottom_half_right - bottom_half_left) / WIDTH);
		c.y = bottom_half_top + (y * (bottom_half_bottom - bottom_half_top) / HEIGHT);

		// Start off z at (0, 0).
		Complex1 z;
		z.x = 0;
		z.y = 0;

		// Iterate z = z^2 + c until z moves more than 2 units
		// away from (0, 0), or we've iterated too many times.
		int iterations = 0;

		while ((z.x * z.x) + (z.y * z.y) < 2.0 * 2.0 && iterations < MAX_ITERATIONS)
		{
			z = c_add(c_mul(z, z), c);

			++iterations;
		}
		if (iterations == MAX_ITERATIONS)
		{

			// z didn't escape from the circle.
			// This point is in the Mandelbrot set.
			bottom_half(y, x) = 0xFFFF0000;
		}
		else
		{
			// z escaped within less than MAX_ITERATIONS
			// iterations. This point isn't in the set.
			//av_image(y, x) = ;
			bottom_half(y, x) = 0xFF000000 + (iterations << 16);
			//av_image(y, x) = colourHSVtoABGR(iterations, 0.8, 0.8);
		}
	});
	concurrency::copy(bottom_half, image_data_2D[int(HEIGHT / accelerators.size())]);
#endif
}

#else
void Mandelbrot::Compute(double left, double right, double top, double bottom)
{
	concurrency::array_view<uint32_t, 2> av_image(HEIGHT, WIDTH, image_data_2D[0]);		// Array view over array...?

	concurrency::parallel_for_each(av_image.extent.tile<TS_Y, TS_X>(),
		[=](concurrency::tiled_index<TS_Y, TS_X> t_idx) restrict(amp)
	{
		int x = t_idx.global[1];
		int y = t_idx.global[0];

		// Work out the point in the complex plane that
		// corresponds to this pixel in the output image.
		Complex1 c;
		c.x = left + (x * (right - left) / WIDTH);
		c.y = top + (y * (bottom - top) / HEIGHT);

		// Start off z at (0, 0).
		Complex1 z;
		z.x = 0;
		z.y = 0;

		// Iterate z = z^2 + c until z moves more than 2 units
		// away from (0, 0), or we've iterated too many times.
		int iterations = 0;

		while ((z.x * z.x) + (z.y * z.y) < 2.0 * 2.0 && iterations < MAX_ITERATIONS)
		{
			z = c_add(c_mul(z, z), c);

			++iterations;
		}
		if (iterations == MAX_ITERATIONS)
		{

			// z didn't escape from the circle.
			// This point is in the Mandelbrot set.
			av_image(y, x) = 0xFFff0000;
		}
		else
		{
			// z escaped within less than MAX_ITERATIONS
			// iterations. This point isn't in the set.
			//av_image(y, x) = ;
			av_image(y, x) = 0xFF000000 + (iterations << 16);
			//av_image(y, x) = colourHSVtoABGR(iterations, 0.8, 0.8);
		}
	});

	av_image.synchronize();
}
#endif

void Mandelbrot::Convert()
{
#if 0
	memcpy(image_data_1D->data(), image_data_2D, WIDTH * HEIGHT * 4);
#else
	int index = 0;
	for (int i = 0; i < HEIGHT; i++)
		for (int j = 0; j < WIDTH; j++)
		{
			sf::Uint8 r = image_data_2D[i][j];
			sf::Uint8 g = image_data_2D[i][j] >> 8;
			sf::Uint8 b = image_data_2D[i][j] >> 16;
			sf::Uint8 a = 0xFF;
			image_data_1D->at(index++) = r;
			image_data_1D->at(index++) = g;
			image_data_1D->at(index++) = b;
			image_data_1D->at(index++) = a;
		}
#endif
}

void Mandelbrot::Render()
{
	window_->clear(sf::Color::White);

	window_->draw(*sprite_);

	window_->display();
}

double Mandelbrot::Map(long double value, long double min1, long double max1, long double min2, long double max2) 
{
	
	return min2 + (max2 - min2) * ((value - min1) / (max1 - min1));
	
}

void Mandelbrot::report_accelerator(const concurrency::accelerator a)
{
	const std::wstring bs[2] = { L"false", L"true" };
	std::wcout << ": " << a.description << " "
		<< std::endl << "       device_path                       = " << a.device_path
		<< std::endl << "       dedicated_memory                  = " << std::setprecision(4) << float(a.dedicated_memory) / (1024.0f * 1024.0f) << " Mb"
		<< std::endl << "       has_display                       = " << bs[a.has_display]
		<< std::endl << "       is_debug                          = " << bs[a.is_debug]
		<< std::endl << "       is_emulated                       = " << bs[a.is_emulated]
		<< std::endl << "       supports_double_precision         = " << bs[a.supports_double_precision]
		<< std::endl << "       supports_limited_double_precision = " << bs[a.supports_limited_double_precision]
		<< std::endl;
}

// List and select the accelerator to use
void Mandelbrot::list_accelerators()
{
	//get all accelerators available to us and store in a vector so we can extract details
	std::vector<concurrency::accelerator> accls = concurrency::accelerator::get_all();

	// iterates over all accelerators and print characteristics
	for (unsigned i = 0; i < accls.size(); i++)
	{
		concurrency::accelerator a = accls[i];
		report_accelerator(a);
		//if ((a.dedicated_memory > 0) & (a.dedicated_memory < 0.5*(1024.0f * 1024.0f)))
		//accelerator::set_default(a.device_path);
	}

	concurrency::accelerator::set_default(accls[ACCELERATOR_ID].device_path);
	concurrency::accelerator acc = concurrency::accelerator(concurrency::accelerator::default_accelerator);
	std::wcout << " default acc = " << acc.description << std::endl;

} 

 // query if AMP accelerator exists on hardware
void Mandelbrot::query_AMP_support()
{
	std::vector<concurrency::accelerator> accls = concurrency::accelerator::get_all();
	if (accls.empty())
	{
		std::cout << "No accelerators found that are compatible with C++ AMP" << std::endl;
	}
	else
	{
		std::cout << "Accelerators found that are compatible with C++ AMP" << std::endl;
		list_accelerators();
	}
} 
