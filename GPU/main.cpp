#pragma once
#include "mandelbrot.h"
#include "input.h"
#include <stdio.h>






int main(int argc, char *argv[])
{
	sf::RenderWindow window_(sf::VideoMode(WIDTH, HEIGHT), "Mandelbrot", sf::Style::Fullscreen);
	window_.setPosition(sf::Vector2i(1920, 0));
	window_.setMouseCursorVisible(false);
	window_.setFramerateLimit(60);

	sf::Mouse mouse;
	mouse.setPosition(sf::Vector2i(window_.getPosition().x + 960, window_.getPosition().y + 540));

	Input input;
	Mandelbrot mandelbrot(&window_, &input);

	while (window_.isOpen())
	{
		if (window_.hasFocus())
		{
			
			sf::Event event;
			while (window_.pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::Closed:
					window_.close();
					break;
				case sf::Event::KeyPressed:
					input.setKeyDown(event.key.code);
					break;
				case sf::Event::KeyReleased:
					input.setKeyUp(event.key.code);
					break;
				case sf::Event::MouseMoved:
					input.setMousePosition(event.mouseMove.x, event.mouseMove.y);
					break;
				case sf::Event::MouseButtonPressed:
					if (event.mouseButton.button == sf::Mouse::Left)
						input.setMouseLeftDown(true);
					if (event.mouseButton.button == sf::Mouse::Right)
						input.setMouseRightDown(true);
					break;
				case sf::Event::MouseButtonReleased:
					if (event.mouseButton.button == sf::Mouse::Left)
						input.setMouseLeftDown(false);
					if (event.mouseButton.button == sf::Mouse::Right)
						input.setMouseRightDown(false);
					break;
				default:
					break;
				}
			}
			mandelbrot.HandleInput();

			//mouse.setPosition(sf::Vector2i(window_.getPosition().x + 320, window_.getPosition().y + 240));

			mouse.setPosition(sf::Vector2i(window_.getPosition().x + 960, window_.getPosition().y + 540));

			if (!mandelbrot.running_)
				window_.close();
		}
	}

	return 0;
}

